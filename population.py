"""
Population movements

cf Verbavatz

https://www.insee.fr/fr/statistiques/2022291
https://www.census.gov/data/tables/2017/demo/geographic-mobility/metro-to-metro-migration.html
https://www.ons.gov.uk/peoplepopulationandcommunity/populationandmigration/migrationwithintheuk/datasets/matricesofinternalmigrationmovesbetweenlocalauthoritiesandregionsincludingthecountriesofwalesscotlandandnorthernireland
https://doi.org/10.25318/1710008701-eng
"""
import pandas as pd
import os
import numpy as np

from network_tools.io import get_adj
from scipy.sparse import csr_matrix

from sknetwork.utils.format import directed2undirected

def test_get_intercity_population_migration_fr():
    """
    """
    
    path_migration= os.environ['HOME']+'/local/data/population_migration/FR/BTT_FM_MRE_2008.txt'
    path_codenames=os.environ['HOME']+'/local/data/insee_COG/commune2021.csv'
    path_epci=os.environ['HOME']+'/local/data/insee_EPCI/Intercommunalite-Metropole_au_01-01-2021.xlsx'

    d = get_intercity_population_migration_fr(directed=False,weighted=False,
                                                            path_migration=path_migration,path_codenames=path_codenames,
                                                            path_epci=path_epci)
    #%matplotlib
    #plt.imshow(adjEPCI.todense())

def get_intercity_population_migration(directed=False,weighted=False,country="fr",fmt="csr",year="2008",
                                                            level=None,path_migration=None,path_codenames=None):
    """
    
    NB: 
    
    directed=False
    
    weighted=False
    
    country="fr"
    
    fmt="csr"
    
    year="2008"
    
    level=None
    """
    pass
    
def get_intercity_population_migration_fr(directed=False,weighted=False,
                                                            path_migration=None,path_codenames=None,
                                                            path_epci=None,
                                                            get_df=False):   
    """
    administrative levels whom interest is uncertain:
    canton: overal 'departement' boundaries ?
    ZE: zone d'emploi. Not all 'communes' are included in a ZE.
    
    TODO:
      * see how merge is done for EPCI: this is quicker than for REG,DEP. => replace old code
      * insee_EPCI  file is enough to get DEP,REG,EPCI !!!!!!!! no need for insee_COG file
      * add more levels ??  (NB: CANTON, BASSIN EMPLOI INSEE: see discussion above)

    """                                                             
    #read migration file
    dtype = {'CODGEO':'str', 'DCRAN':'str','L_DCRAN':'str',' NBFLUX_C08_POP05P':  np.float64}
    df_migration = pd.read_csv(path_migration,sep=";",header=0,encoding="ISO-8859-1",dtype=dtype,
                                                decimal=',')#thousands=",",engine='python')    
    #read codename file (for 'region' and 'departement')
    df_codename = pd.read_csv(path_codenames,sep=",",header=0,encoding="ISO-8859-1",
                                                dtype={'REG':'str','COM':'str','DEP':'str'})
    #join: get source departement and region
    df_merge = pd.merge(df_migration.rename(columns={'CODGEO':'COM'}),
                                   df_codename[['COM','DEP','REG']],
                                   on='COM')  
    df_merge.rename(columns={'DEP':'DEP_source','REG':'REG_source','COM':'CODGEO'},inplace=True)                                   
    #join: get destination departement and region
    df_merge = pd.merge(df_merge.rename(columns={'DCRAN':'COM'}),
                                   df_codename[['COM','DEP','REG']],
                                   on='COM')  
    df_merge.rename(columns={'DEP':'DEP_destination','REG':'REG_destination',
                                                  'CODGEO':'COM_source', 'COM':'COM_destination' },inplace=True)                                   
    #read EPCI     
    if path_epci is not None :
        dtype = {'CODGEO':'str', 'LIBGEO':'str', 'EPCI':'str', 'LIBEPCI':'str', 'DEP':'str', 'REG':'str' }
        df_epci = pd.read_excel(path_epci, sheet_name= 'Composition_communale',
                                              skiprows=[0,1,2,3,4],header=0 ,
                                              dtype=dtype )
        df_merge = pd.merge( df_merge,
                            df_epci[['CODGEO','EPCI']].rename(columns={'CODGEO':'COM_source', 'EPCI':'EPCI_source'})  )                                      
        df_merge = pd.merge( df_merge,
                            df_epci[['CODGEO','EPCI']].rename(columns={'CODGEO':'COM_destination', 'EPCI':'EPCI_destination'})  )                                                                  
        df_merge.dropna(axis=0,subset=['EPCI_source','EPCI_destination'],  inplace=True)                    
    #clean
    df_merge.dropna(axis=0,subset=['COM_source','COM_destination','REG_source','REG_destination'],
                                inplace=True)
    #get csr matrix 
    adj_COM,label_COM = get_adj(df_merge,'COM_source', 'COM_destination',weighted=weighted)
    adj_DEP,label_DEP = get_adj(df_merge,'DEP_source', 'DEP_destination',weighted=weighted)
    adj_REG,label_REG = get_adj(df_merge,'REG_source', 'REG_destination',weighted=weighted)
    adj_EPCI,label_EPCI = get_adj(df_merge,'EPCI_source', 'EPCI_destination',weighted=weighted)
    #directedness
    if not directed:
        adj_COM = directed2undirected(adj_COM, weighted= weighted)
        adj_EPCI = directed2undirected(adj_EPCI, weighted= weighted)
        adj_DEP = directed2undirected(adj_DEP, weighted= weighted)
        adj_REG = directed2undirected(adj_REG, weighted= weighted)
    #diag
    adj_COM.setdiag(0)    
    adj_EPCI.setdiag(0)    
    adj_DEP.setdiag(0)    
    adj_REG.setdiag(0)
    #create a dict 
    d = { 0:{'n_node':  len(label_COM), 'adjacency': adj_COM, 'names':label_COM},
             1:{'n_node':  len(label_EPCI), 'adjacency': adj_EPCI, 'names':label_EPCI},
             2:{'n_node':  len(label_DEP), 'adjacency': adj_DEP, 'names':label_DEP},
             3:{'n_node':  len(label_REG), 'adjacency': adj_REG, 'names':label_REG}
              }
    if get_df:
        return d,df_merge,df_epci
    else:    
        return d
