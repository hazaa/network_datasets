"""
load BACI dataset, create a igraph Graph, create a dendrogram using 
inter-country distances

HS96 (1996-2018, 2.28 Go)    http://www.cepii.fr/DATA_DOWNLOAD/baci/trade_flows/BACI_HS96_V202001.zip
"""

import pandas as pd
import numpy as np
import os

import igraph as ig 


def test_get_graph_ig():
    """
    """
    #from network_datasets.baci import *
    fname_edge = os.environ['HOME']+'/local/data/baci/BACI_HS96_Y2011_V202001_ij.csv'
    fname_country_names =  os.environ['HOME']+'/local/data/baci/BACI_country_codes_V202001.csv'
    directed=False
    g = get_graph_ig(fname_edge,fname_country_names,directed=directed)
    assert True    

def get_graph_ig(fname_edge,fname_country_names=None,directed=False):
    """
    get binary graph
    using pd.DataFrame
    """
    df_edge = pd.read_csv(fname_edge,sep=' ' ,names=['country_code_o',
                'country_code_d']).drop_duplicates(ignore_index=True)
    if fname_country_names is None :
        g = ig.Graph.DataFrame(df_edge)
    else:
        df_countries = pd.read_csv(fname_country_names,sep=',',  header=0,
                                    engine='python')    
        # check all edges have a corresponding name
        node_unique_from_edge = pd.concat( [df_edge['country_code_o'] , df_edge['country_code_d']] ).unique()
        node_unique_from_country = df_countries['country_code'].unique()                        
        isin = np.isin(node_unique_from_edge, node_unique_from_country)
        assert np.all(isin)
        # recode df_edge with country names                              
        df_countries = df_countries[['country_code','iso_3digit_alpha']]                            
        df_merge = pd.merge(df_edge,df_countries.rename(columns={'country_code':'country_code_o'}))
        df_merge.rename(columns={'iso_3digit_alpha':'iso_o'} ,inplace=True)
        df_merge = pd.merge(df_merge,df_countries.rename(columns={'country_code':'country_code_d'}))
        df_merge.rename(columns={'iso_3digit_alpha':'iso_d'} ,inplace=True)
        df_merge = df_merge[['iso_o', 'iso_d']]
        df_merge.dropna(inplace=True)
        #
        g=ig.Graph.TupleList(df_merge.to_numpy().tolist())
        """
        FAILS
        g = ig.Graph.DataFrame(df_merge, 
                    pd.DataFrame(node_unique_from_edge)) # okbut node names not set !!!
        g.vs['name'] = ['']*g.vcount()
        for e in g.es:
            o = e.source_vertex
            o['name']=e['iso_o']
            d = e.target_vertex['name']
            d['name'] = e['iso_d']
            """
    #remove loops multiple edges
    g.simplify(multiple=True, loops=True)
    return g    
    

def get_graph_ig_old(fname_edge,fname_country_names=None,directed=False):
    """
    get an igraph Graph from edge file
    
    
    refs:
        Read_Edgelist : https://igraph.org/python/doc/igraph.GraphBase-class.html#Read_Edgelist
    """
    #g = ig.Graph.Load(fname, format='edgelist')
    g = ig.Graph.Read_Edgelist(fname_edge,directed=directed)
    g.simplify(multiple=True, loops=True)

    # read country names
    if not fname_country_names is None :
        #df_countries = pd.read_csv(fname_country_names,sep=',')
        df_countries = pd.read_csv(fname_country_names,sep=',',  header=0,
                                    engine='python')
        for i in range(df_countries.shape[0]):
            country_code = df_countries.country_code[i]
            iso = df_countries.iso_3digit_alpha[i]
            g.vs[country_code]['iso'] = iso
            g.vs[country_code]['country_code']=country_code 
    return g

def test_get_unique_iso_dist_cepii():
    """
    """
    fname_dist = os.environ['HOME']+'/local/data/baci/dist_cepii.dta'
    iso = get_unique_iso_dist_cepii(fname_dist)
    

def get_unique_iso_dist_cepii(fname):
    """
    get unique iso country name appearing in dist_cepii files
    """
    df_dist = pd.read_stata(fname)
    return pd.concat( [df_dist['iso_o'], df_dist['iso_d'] ]).unique()    

def test_get_dist_flat():
    """
    """
    fname_dist = os.environ['HOME']+'/local/data/baci/dist_cepii.dta'
    iso = get_unique_iso_dist_cepii(fname_dist)
    n_iso = len(iso)
    dists = get_dist_flat(iso,fname_dist)
    assert dists.shape[0] == int(n_iso*(n_iso-1))/2
    assert not np.any(np.isnan(dists))

def condensed_index(n, i, j):
    """
    Calculate the condensed index of element (i, j) in an n x n condensed
    matrix.
    
    copied from https://github.com/scipy/scipy/blob/v1.6.0/scipy/cluster/_hierarchy.pyx
    """
    if i < j:
        return int(n * i - (i * (i + 1) / 2) + (j - i - 1))
    elif i > j:
        return int(n * j - (j * (j + 1) / 2) + (i - j - 1) )


def dist_cepii_DESIRE_extend():
    """
    to be used after lca.py:mrio_dist_chunk() was run
    index.get_level_values('region').unique()
    
    df_dist = pd.read_stata(fname)
    
    RoW_list = [WA,WE,..]
    for iso_ in iso3:
       for RoW_ in RoW_list:
            RoW_members = see CountryMappingDESIRE.xlsx
            join df_dist[['iso_o', 'iso_d']]  with iso_o=iso_ and iso_d in RoW_members
            compute median
    save in new file
    """
    pass

def get_dist_flat(iso,fname=None,df_dist=None):
    """
    get inter country distance in dist_cepii files, in the form
    of a flat array compatible with hierarchical clustering algorithms.
    
    https://docs.scipy.org/doc/scipy/reference/generated/scipy.spatial.distance.pdist.html#scipy.spatial.distance.pdist
    https://github.com/scipy/scipy/blob/v1.6.0/scipy/cluster/_hierarchy.pyx
    https://docs.scipy.org/doc/scipy/reference/generated/scipy.cluster.hierarchy.linkage.html#scipy.cluster.hierarchy.linkage
    """
    n_iso = len(iso)
    if df_dist is None:
        df_dist = pd.read_stata(fname)
    # create MultiIndex, and lookup Series
    # https://pandas.pydata.org/docs/user_guide/advanced.html#creating-a-multiindex-hierarchical-index-object
    index = pd.MultiIndex.from_frame( df_dist[['iso_o', 'iso_d']])
    lookup = df_dist['dist']
    lookup.index = index
    #test: assert lookup['ABW','ALB']==9091.742
    # encode pairwise distance in a 1d array
    # dist(u=X[i], v=X[j]) is stored in entry m * i + j - ((i + 2) * (i + 1)) // 2
    # https://docs.scipy.org/doc/scipy/reference/generated/scipy.spatial.distance.pdist.html#scipy.spatial.distance.pdist
    dists = np.zeros( int(n_iso*(n_iso-1)/2) )
    for i in range(n_iso-1):
        for j in range(i+1,n_iso):
            orig = iso[i]
            dest = iso[j]
            k = condensed_index(n_iso,i,j)
            dists[k] = lookup[orig,dest]
    return dists
