"""
DATABASE
* MR EE SUT/IOT https://nexus.openlca.org/database/exiobase



PREPROCESSING DENSE MATRIX->SPARSE MATRIX:
* see gemmetto for review backbone (gloss,...)
* disparity filter: code
  speed ?
  gemmetto filter : removes 'expected' nodes, leaves 'unexpected' ones. 
                    Is it what we want ?
        (+ fast CM solver squartini https://arxiv.org/abs/2101.12625v1 )
           
* Polya Urn filter: https://fr.mathworks.com/matlabcentral/fileexchange/69501-pf              
* TODO: compare effet of backbone on: sum(weight), nb node [cf polya urn ; XU lian ]
* backbone + bipartite projection: https://www.sciencedirect.com/science/article/pii/S0378873314000343
  fast poisson https://arxiv.org/pdf/1512.01883

GRAPH CONSTRUCTION PRINCIPLE

* bipartite projection. Process-based/product-based ? (cf heijungs15)
 "We used version v2.2, and took the allocated dataset with infrastructure"

DENDROGRAM CONSTRUCTION PRINCIPLE

* predefine clusters: not a prerequisite with this method.
* per-industry dendrogram (e.g. 1 economy, official sector classification).
* political hierarchy: country converter
        https://github.com/konstantinstadler/country_converter
* define a distance measure 
    * for sparse net define embedding, then compute dist
      (low-mem issue)
    * for dense mat: define and compute distance between rows. 
                    cpu/memory issue: slow and 
* all industries distances refer to the country to which they belong
  PROBLEM: first agg will reflect only country distance.
* per-industry dendrogram (e.g. 1 economy, product hierarchical nomenclature)
        AND per country dist ??
    

REFERENCES

"""
import numpy as np
import pandas as pd
import pymrio
    
def trade_by_product():
    """
    Q: some products networks better fit renormalizable model than others ? if yes why ?
    """
    pass
    
def io_sectors():
    """
    single region. Several sector classification levels
    e.g. NACE2full
    correspondence: 
    exiobase https://ntnu.app.box.com/s/ziox4zmkgt3cdsg549brr0qaecskgjsd?page=1
    """
    pass    


def test_mrio_dist_chunk():
    """
    """
    fname = "/home/aurelien/local/data/MRIO/exiobase_3.4_iot_2011_ixi.current/Z.txt"
    
    coo,index = mrio_dist_chunk(fname,filt="thr",filt_param=0.01, db="exiobase3",
                    chunksize=1000,plot=False)
    csr = coo.tocsr()                
    g =   ig.Graph.TupleList( zip(coo.row,coo.col,coo.data)  )             
    assert True
            


def mrio_dist_chunk(fname,filt="thr",filt_param=0.01, db="exiobase3",
                    chunksize=1000,plot=False):
    """
    get sparse COO matrix from MRIO text file
    chunk by chunk to limit memory overhead
    
    unit is million euro in exiobase3.
    
    Refs: 
     pd.sdf to  COO: https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.sparse.to_coo.html#pandas.DataFrame.sparse.to_coo
     definition COO: https://docs.scipy.org/doc/scipy/reference/generated/scipy.sparse.coo_matrix.html#scipy.sparse.coo_matrix
    
    """
    if db=="exiobase3":
        nr_index_col = 2
        nr_header = 2      
        _index_col = list(range(int(nr_index_col)))
        _header = list(range(int(nr_header)))
    else : 
        raise NotImplementedError
        
    reader = pd.read_csv(fname, index_col=_index_col, header=_header,
                            sep='\t',chunksize=chunksize)  
    i=0
    sdf=pd.DataFrame()                     
    for df in reader:                           
            #df = reader.get_chunk(chunksize)                             
        if plot:
            hist, bin_edges=np.histogram(df.values,np.logspace(-3,2,50 ),density=True)
            plt.semilogx(bin_edges[:-1],hist)
        print('dense : {:0.2f} bytes'.format(df.memory_usage(deep=True).sum() / 1e6) )
        # filter
        if filt=="thr": 
            threshold = filt_param
            df[df<threshold]=0 
            #https://pandas.pydata.org/docs/user_guide/sparse.html?highlight=sparsedatafram#sparse-data-structures
            if i==0:
                sdf = df.astype(pd.SparseDtype("float", 0)) 
            else:
                sdf = sdf.append(df.astype(pd.SparseDtype("float", 0)) )    
            print('sdf.sparse.density={}'.format(sdf.sparse.density))
            print('dense trim: {:0.2f} bytes'.format(df.memory_usage(deep=True).sum() / 1e6))
            print('sparse: {:0.2f} bytes'.format(sdf.memory_usage(deep=True).sum() / 1e6))
        elif disparity:
            raise NotImplementedError
        elif polya:
            raise NotImplementedError
        else: raise ValueError('unknown filt value')
        i+=1    
    return sdf.sparse.to_coo(),sdf.index
    
def mrio_dist(db="exiobase"):
    """
    Q: heijungs fait IO nationale, pas *MR*IO. => il trouve dense.
          
    PYMRIO
    https://pymrio.readthedocs.io/en/latest/notebooks/working_with_exiobase.html#Getting-EXIOBASE
    https://github.com/konstantinstadler/pymrio
    
    * EXIOBASE:  44 countries, 5 Rest of World regions 200 products 163 industries
            NB: a priori ça rentre dans framework garla
            Merciai, S. and J. Schmidt (2018) Methodology for the Construction of Global Multi-Regional Hybrid Supply and Use Tables for the EXIOBASE v3 Database. Journal of Industrial Ecology, 22(3)516-531. doi:10.1111/jiec.12713
    
    * WIOD        
    * EORA ? 
         IIOT: industry-to-industry    CIOT: Commodity-by-Commodity
         (NB: supply-use: industry->commodity / commodity->industry == BIPARTITE!!!)     
    * Material Flow:   Nuss et al.
    * (Nested MRIO tables ???
    """
    """
    # exiobase
     unit = 1E6 € (see unit.txt)
     Z.txt = transaction matrix (not A)
     # chunk reading 
     # https://pandas.pydata.org/docs/user_guide/io.html#iterating-through-files-chunk-by-chunk
     fname = "/home/aurelien/local/data/MRIO/exiobase_3.4_iot_2011_ixi.current/Z.txt"
     df = pd.read_csv(file, dtype=np.float, skiprows=2)   ??
     # first pass (if necessary, to get marginals)

     with pd.read_table(fname,  chunksize=4, skiprows=2) as reader:
        for i,chunk in enumerate(reader):
            print(chunk)
            if i>5: break   # FAILS
      reader =pd.read_table(fname,  iterator=True)  
      while True:      
         df = reader.get_chunk(100)
     # !!!!!!!!TODO: df=pd.read_csv(fname, usecols=range(2,6000), skiprows=2, dtype=np.float32)
     df=pd.read_table(fname, names=range(0,7988),usecols=range(2,7988), 
                    skiprows=2, dtype=np.float32) 
    """
    #if db=='exiobase':
    exio3 = pymrio.parse_exiobase3(path='/home/aurelien/local/data/MRIO/exiobase_3.4_iot_2011_ixi.zip')
    exio3.meta
    exio3.get_sectors()
    exio3.get_regions()
    list(exio2.get_extensions())
    import matplotlib.pyplot as plt
    plt.figure(figsize=(15,15))
    plt.imshow(exio2.A, vmax=1E-3)
    plt.xlabel('Countries - sectors')
    plt.ylabel('Countries - sectors')
    plt.show()
    # WIOD
    #https://pymrio.readthedocs.io/en/latest/notebooks/working_with_wiod.html
    wiod_folder ="/home/aurelien/local/data/MRIO/wiod13"
    wiod_meta = pymrio.download_wiod2013(storage_folder=wiod_folder)
    print(wiod_meta)
    wiod2007 = pymrio.parse_wiod(year=2007, path=wiod_folder)
    wiod2007.Z.head()
    wiod2007_full = pymrio.parse_wiod(year=2007, path=wiod_folder, names=('full', 'full'))
    wiod2007_full.Y.head()
    # NOT ENOUGH MEMORY !!!!!!!!!!!!!!!!!!!!!!
    # read distances 
    # set distance to blocks to +inf
    # compute dendrogram
    # compute renormalized model
    # compare with backbone
    pass

def eora(storage,year=2012):
    """
    http://www.worldmrio.com
    https://pymrio.readthedocs.io/en/latest/notebooks/working_with_eora26.html
    """
    eora = pymrio.parse_eora26(year=year, path=eora_storage)
    return eora.Z


def rail_transport():
    """
    from OD matrix
    ???TODO: move to population.py ???
    """
    pass


"""
others ?
"""
