#https://stackoverflow.com/questions/29167727/how-to-download-a-csv-file-from-the-world-banks-dataset
import os
import urllib
import zipfile
from io import BytesIO
import pandas as pd

"""
get dataframes from worldbank open data

https://blogs.worldbank.org/opendata/first-steps-in-integrating-open-data
"""

def test_get_csv_api():  
    """
    """
    dest_path = os.environ['HOME']+'/local/git/network_datasets/data'
    req = "http://api.worldbank.org/v2/en/indicator/NY.GDP.MKTP.CD?downloadformat=csv"
    get_csv_api(dest_path,req = "http://api.worldbank.org/v2/en/indicator/NY.GDP.MKTP.CD?downloadformat=csv")  
    assert True

def test_get_yearly_gdp_from_df():
    fname = os.environ['HOME']+'/local/git/network_datasets/data/API_NY.GDP.MKTP.CD_DS2_en_csv_v2_1926685.csv'
    df = get_df_from_csv(fname  )
    year = 2011
    df2 = get_yearly_gdp_from_df(df,year)
    assert True

def get_yearly_gdp_from_df(df,year):
    """
    get gdp of all countries for specific year    
    """
    year = str(year)
    df = df[['Country Name', 'Country Code', year]]
    return df.rename(columns={year:'GDP'})


def get_csv_api(dest_path,req = "http://api.worldbank.org/v2/en/indicator/NY.GDP.MKTP.CD?downloadformat=csv") :  
    """
    get csv from api.worldbank.org
    """
    #package = StringIO(urllib.request.urlopen(req).read())
    package = BytesIO(urllib.request.urlopen(req).read())
    zip = zipfile.ZipFile(package, 'r')
    #pwd = os.path.abspath(os.curdir)

    for filename in zip.namelist():
        csv = os.path.join(dest_path, filename)
        with open(csv, 'wb') as fp:
            fp.write(zip.read(filename))
        #print filename, 'downloaded successfully'

def test_get_df_from_csv():
    """
    """
    fname = os.environ['HOME']+'/local/git/network_datasets/data/API_NY.GDP.MKTP.CD_DS2_en_csv_v2_1926685.csv'
    df = get_df_from_csv(fname  )
    assert True

def get_df_from_csv(fname ):
    """
    """
    df = pd.read_csv(fname, sep=',', lineterminator='\n',skiprows=4)
    return df

def get_df_from_excel():
    """
    """
    req = "http://api.worldbank.org/v2/en/indicator/NY.GDP.MKTP.CD?downloadformat=excel"
    return  pd.read_excel(req)

