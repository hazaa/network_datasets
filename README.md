# network_datasets

Various tools to setup network datasets.

## Network datasets repositories

* karate club:

## Baci

* get data : 
   *   HS96 (1996-2018, 2.28 Go), [www](http://www.cepii.fr/DATA_DOWNLOAD/baci/trade_flows/BACI_HS96_V202001.zip)
   *   country names: 
   *   geodist : 
* preprocess steps are necessary. [csvkit](https://csvkit.readthedocs.io/) is used: keep 2 columns, remove header, change delimiter ","->" ".  TODO: remove duplicate lines ??            
  ```pipenv run csvcut -d "," --skip-lines 1 -c 2,3  /home/aurelien/local/data/baci/BACI_HS96_Y2011_V202001.csv |pipenv run csvformat -D " " > /home/aurelien/local/data/baci/BACI_HS96_Y2011_V202001_ij.csv```
* Usage: see [baci.py](baci.py)
    ```
    g = get_graph_ig(fname_edge,fname_country_names)
    ```

## World Bank

* dataset NY.GDP.MKTP.CD_DS2
* [databank](https://databank.worldbank.org)
* Usage: see [worldbank.py](worldbank.py)



## Exiobase

* [CountryMappingDESIRE.xlsx](https://ntnu.app.box.com/s/ziox4zmkgt3cdsg549brr0qaecskgjsd/file/540136000130)
* [exiobase 3 monetary](https://exiobase.eu/index.php/data-download/exiobase3mon)

## LCA

### openLCA

* process [schema](http://greendelta.github.io/olca-schema/Process.html) with exported process in JSON-LD format. (see also [process.json](https://github.com/GreenDelta/olca-schema/blob/master/examples/process.json))
* java code [ProcessWriter.java](https://github.com/GreenDelta/olca-modules/blob/master/olca-core/src/main/java/org/openlca/jsonld/output/ProcessWriter.java) that writes the JSON.
* database schema used for persistence of java objects [current_schema_derby.sql](https://github.com/GreenDelta/olca-modules/blob/master/olca-core/src/main/resources/org/openlca/core/database/internal/current_schema_derby.sql)
```sql
CREATE TABLE tbl_exchanges (

    id                        BIGINT NOT NULL,
    f_owner                   BIGINT,
    internal_id               INTEGER,
    f_flow                    BIGINT,
    f_unit                    BIGINT,
    is_input                  SMALLINT default 0,
    f_flow_property_factor    BIGINT,
    resulting_amount_value    DOUBLE,
    resulting_amount_formula  VARCHAR(1000),
    avoided_product           SMALLINT default 0,
    f_default_provider        BIGINT,
    f_location                BIGINT,
    description               CLOB(64 K),

    cost_value                DOUBLE,
    cost_formula              VARCHAR(1000),
    f_currency                BIGINT,

    distribution_type         INTEGER default 0,
    parameter1_value          DOUBLE,
    parameter1_formula        VARCHAR(1000),
    parameter2_value          DOUBLE,
    parameter2_formula        VARCHAR(1000),
    parameter3_value          DOUBLE,
    parameter3_formula        VARCHAR(1000),

    dq_entry                  VARCHAR(50),
    base_uncertainty          DOUBLE,

    PRIMARY KEY (id)

);
```
